#!/usr/bin/env python
import pygame, os, sys, time 
from pygame.locals import *

pygame.init()
fpsClock = pygame.time.Clock()
screen_width = 800
screen_height = 600
surface = pygame.display.set_mode((screen_width, screen_height))
distance_wall = 100
distance_width = screen_width - distance_wall
distance_height = screen_height - distance_wall
background = pygame.Color(100, 149, 237)
image = pygame.image.load('canvas.png')
meeple1 = pygame.image.load('eyelander.png')
player1_x = 100 
player1_y = 100
meeple2 = pygame.image.load('Snake.png')
player2_x = distance_width
player2_y = distance_height

pygame.font.init()
myfont = pygame.font.SysFont("arial", 25)


while True:
    surface.fill(background)
    surface.blit(image, (0,0))
    surface.blit(meeple1, (player1_x, player1_y))
    surface.blit(meeple2, (player2_x, player2_y))
    textsurface = myfont.render('Hightscore: ', False, (0, 0, 0))
    surface.blit(textsurface, (0,0))
    for event in pygame.event.get():
        # print(player1_x, player1_y)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                player1_x = 0
                player1_y = 0
            elif event.key == pygame.K_RIGHT\
            and player1_x < distance_width:
                player1_x += 100
            elif event.key == pygame.K_LEFT\
            and player1_x > distance_wall:
                player1_x -= 100
            elif event.key == pygame.K_DOWN\
            and player1_y < distance_height:
                player1_y += 100
            elif event.key == pygame.K_UP\
            and player1_y > distance_wall:
                player1_y -= 100
            if event.key == pygame.K_d\
            and player2_x < distance_width:
                player2_x += 100
            elif event.key == pygame.K_a\
            and player2_x > distance_wall:
                player2_x -= 100
            elif event.key == pygame.K_s\
            and player2_y < distance_height:
                player2_y +=100
            elif event.key == pygame.K_w\
            and player2_y > distance_wall:
                player2_y -= 100
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
    fpsClock.tick(30)

