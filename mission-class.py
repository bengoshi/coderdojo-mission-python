#!/usr/bin/python3
# Code by Cobalt <https://github.com/Chaostheorie>

from pygame.locals import *
import pygame
import os
import sys

pygame.init()

class BaseSprite(pygame.sprite.Sprite):
    def __init__(self, x: int, y: int):
        pygame.sprite.Sprite.__init__(self)
        self.movex = 0
        self.movey = 0
        self.frame = 0
        self.image = pygame.image.load("eyelander.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def move(self, x: int, y: int) -> bool:
        return True

    def update(self) -> None:
        self.rect.x = self.rect.x + self.movex
        self.rect.y = self.rect.y + self.movey
        return

    def check_move(self, event) -> None:
        return


class MeepleSpirit(BaseSprite):
    visible = 1

    def move(self, x: int, y: int) -> None:
        self.movey = y
        self.movex = x

    def check_move(self, event) -> None:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                self.move(100, 0)
            elif event.key == pygame.K_LEFT:
                self.move(-100, 0)
            elif event.key == pygame.K_DOWN:
                self.move(0, 100)
            elif event.key == pygame.K_UP:
                self.move(0, -100)
        print("check_move")
        return


pygame.init()
fpsClock = pygame.time.Clock()
surface = pygame.display.set_mode((800, 600))
background = pygame.Color(100, 149, 237)
global_group = pygame.sprite.Group(MeepleSpirit(0, 0))
surface.fill(background)
global_group.draw(surface)
pygame.display.flip()
while True:
    for event in pygame.event.get():
        [sprite.check_move(event) for sprite in global_group.sprites()]
        global_group.update()
        surface.fill(background)
        global_group.draw(surface)
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    pygame.display.flip()
    pygame.display.update()
    fpsClock.tick(30)

