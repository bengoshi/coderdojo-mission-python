#!/usr/bin/env python
import pygame, os, sys 
from pygame.locals import *

pygame.init()
fpsClock = pygame.time.Clock()
screen_width = 800
screen_height = 600
surface = pygame.display.set_mode((screen_width, screen_height))
distance_wall = 100
distance_width = screen_width - distance_wall
distance_height = screen_height - distance_wall
background = pygame.Color(100, 149, 237)
image = pygame.image.load('canvas.png')
meeple = pygame.image.load('eyelander.png')
player_x = 100 
player_y = 100 

while True:
    surface.fill(background)
    surface.blit(image, (0,0))
    surface.blit(meeple, (player_x, player_y))
    for event in pygame.event.get():
        print(player_x, player_y)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                player_x = 0
                player_y = 0
            if event.key == pygame.K_RIGHT\
            and player_x < distance_width:
                player_x += 100
            if event.key == pygame.K_LEFT\
            and player_x > distance_wall:
                player_x -= 100
            if event.key == pygame.K_DOWN\
            and player_y < distance_height:
                player_y += 100
            if event.key == pygame.K_UP\
            and player_y > distance_wall:
                player_y -= 100
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
    fpsClock.tick(30)

